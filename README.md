

# Chip Code Challenge

- Api link Dogs API (https://dog.ceo/dog-api/)

## Requirement

As a user running the application I can select breed from the list So that I can view pictures of that breed

Scenario: Viewing the breed list When I launch the app Then I see a list of dog breeds

Scenario: Viewing pictures of breed Given I have launched the app When I select a breed from the list Then I see 10 images of the breed

## Technologies Used
- Room for database
- Retrofit and OkHTTP for making api calls
- koin for dependency injection
- Coil for image cache
- Junit and Mockk for testing
- Jetpack Compose - interoperable
- Architecture - clean Architecture is used

## Note

When it comes to Compose, the reason i decided to go with interoperable rather than fully with compose is for two reasons,
-1. As discussed on the first stage of the interview, the team is planning to migrate from existing code base with using compose and it should be done incrementally and phase by phase, so here i used very minor XML or No-XML to show how it can be used.
-2. In my personal opinion and there are few who also share my concern, Compose navigation is bit complex and not as robust as expected and adopting it might not be a the best solution. Here are few brain teasers [why-using-navigation-compose-in-your-jetpack-compose-app-is-a-bad-idea](https://www.droidcon.com/2022/02/23/why-using-navigation-compose-in-your-jetpack-compose-app-is-a-bad-idea/)  
 Although the requirement is to display 10 images, the api doesn't always return 10 image urls for few breeds, so i have added a simple placeholder for the photo gallery and it fills any remaining empty spaces. eg australian, dalmatian etc...

## Further adjustments
- code cleanup and add multiple test cases
- polish the ui
- add UI test
- lint checks etc..

***

## Installation
installation should be simple, just clone the project in android studio and run the project, if not compiling or have issues may be updating android studio to Iguana could solve it.
