package com.chip.code_challenge.dogs.presentation

import android.app.Application
import androidx.lifecycle.Observer
import com.chip.code_challenge.dogs.MainDispatcherRule
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import com.chip.code_challenge.dogs.di.apiModule
import com.chip.code_challenge.dogs.di.databaseModule
import com.chip.code_challenge.dogs.di.dogsRepoModule
import com.chip.code_challenge.dogs.di.viewModelModule
import com.chip.code_challenge.dogs.domain.usecases.GetSavedBreedPhotosUseCase
import com.chip.code_challenge.dogs.domain.usecases.GetSavedBreedsUseCase
import com.chip.code_challenge.dogs.utils.Const
import com.chip.code_challenge.dogs.utils.ResultWrapper
import com.chip.code_challenge.dogs.utils.getOrAwaitValue
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import org.koin.test.mock.MockProviderRule
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class DogsViewModelTest : KoinTest{

    private val mockApplication: Application = mockk()

    private val getSavedBreedsUseCase: GetSavedBreedsUseCase = mockk()

    private val getSavedBreedPhotosUseCase: GetSavedBreedPhotosUseCase= mockk()

    private val viewModel : DogsViewModel by inject()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()


    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        mock(clazz.java)
    }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        androidContext(mockApplication)
        modules(listOf(apiModule,databaseModule,
            dogsRepoModule, viewModelModule, module { single { mockApplication } }))
    }

    @Test
    fun testGetSavedBreedPhotosFromDb() = runTest {
        // Mock data and dependencies
        val breed = "Labrador"
        val photoAmount = "5"
        val mockBreedPhotos = BreedPhotoEntity("Labrador", listOf("url1", "url2"))

        every { getSavedBreedPhotosUseCase.invoke(breed) } returns flowOf(mockBreedPhotos)

        // Call the method to be tested
        viewModel.getSavedBreedPhotosFromDb(breed, photoAmount)

        // Delay to allow coroutines to execute
        delay(100)


        assertEquals(mockBreedPhotos, viewModel.breedsImage.getOrAwaitValue())
    }

    @Test
    fun `getSavedBreedsFromDB when empty should update LiveData with Error`() = runTest {
        every { getSavedBreedsUseCase.invoke() } returns flowOf(emptyList())

        val observer = mock<Observer<ResultWrapper<List<BreedEntity>>>>()
        viewModel.dogBreeds.observeForever(observer)

        viewModel.getSavedBreedsFromDB()

        assertEquals(ResultWrapper.Error(Const.NETWORK_ERROR), viewModel.dogBreeds.value)
    }

    @Test
    fun `getSavedBreedsFromDB with data should update LiveData with Success`() = runTest {

        val breeds = listOf(BreedEntity("Labrador", listOf("sub_breed"))
            , BreedEntity("Beagle", listOf("sub_breed")))

        every { getSavedBreedsUseCase.invoke() } returns flowOf(breeds)
        val observer = mock<Observer<ResultWrapper<List<BreedEntity>>>>()
        viewModel.dogBreeds.observeForever(observer)

        viewModel.getSavedBreedsFromDB()

        assertEquals(ResultWrapper.Success(breeds), viewModel.dogBreeds.value)
    }

}