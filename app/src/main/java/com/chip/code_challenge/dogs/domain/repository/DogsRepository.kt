package com.chip.code_challenge.dogs.domain.repository

import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.data.local.model.BreedImageResponse
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import com.chip.code_challenge.dogs.data.local.model.DogsResponse
import com.chip.code_challenge.dogs.utils.ResultWrapper
import kotlinx.coroutines.flow.Flow

interface DogsRepository {

     fun fetchAllDogBreedsFromApi() : Flow<ResultWrapper<DogsResponse>>

     fun fetchBreedPhotosFromApi(breed : String, photoAmount : Int) : Flow<ResultWrapper<BreedImageResponse>>

     suspend fun saveAllBreedsToDatabase(breeds : List<BreedEntity>)

     suspend fun saveAllBreedPhotosToDatabase(breedPhotoEntity: BreedPhotoEntity)

     fun getSavedBreedsFromDatabase() : Flow<List<BreedEntity>>

     fun getSavedBreedPhotosFromDatabase(breed: String) : Flow<BreedPhotoEntity?>
}