package com.chip.code_challenge.dogs.di

import com.chip.code_challenge.dogs.domain.repository.DogsRepository
import com.chip.code_challenge.dogs.data.repository.DogsRepositoryImpl
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.bind
import org.koin.dsl.module
import com.chip.code_challenge.dogs.domain.usecases.*



val dogsRepoModule = module {

    singleOf(::DogsRepositoryImpl) { bind<DogsRepository>()}
    singleOf(::FetchAllBreedsFromApiUseCase)
    singleOf(::FetchBreedPhotosFromApiUseCase)
    singleOf(::GetSavedBreedsUseCase)
    singleOf(::GetSavedBreedPhotosUseCase)
    singleOf(::SaveBreedsUseCase)
    singleOf(::SaveBreedPhotosUseCase)
}