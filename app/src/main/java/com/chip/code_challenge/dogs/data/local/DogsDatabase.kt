package com.chip.code_challenge.dogs.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.chip.code_challenge.dogs.data.local.convertors.DogsDBTypeConvertor
import com.chip.code_challenge.dogs.data.local.dao.BreedPhotoDao
import com.chip.code_challenge.dogs.data.local.dao.BreedsDao
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity


@Database(
    entities = [BreedEntity::class,BreedPhotoEntity::class],
    version = 1,
    exportSchema = false)

@TypeConverters(DogsDBTypeConvertor::class)
abstract class DogsDatabase : RoomDatabase(){
    abstract val breedsDao : BreedsDao
    abstract val breedPhotoDao : BreedPhotoDao
}
