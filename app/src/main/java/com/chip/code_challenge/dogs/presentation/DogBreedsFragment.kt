package com.chip.code_challenge.dogs.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import com.chip.code_challenge.dogs.R
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.presentation.ui.DisplayAllDogBreeds
import com.chip.code_challenge.dogs.presentation.ui.DisplayLoading
import com.chip.code_challenge.dogs.presentation.ui.EmptyScreen
import com.chip.code_challenge.dogs.utils.Const
import com.chip.code_challenge.dogs.utils.ResultWrapper
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class DogBreedsFragment : Fragment() {

    private val viewModel : DogsViewModel by activityViewModel()

    companion object {
        @JvmStatic
        fun dogBreedInstance() = DogBreedsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val breeds = viewModel.dogBreeds.observeAsState().value
                ObserveDogsState(breeds = breeds)
            }
        }
    }

    @Composable
    private fun ObserveDogsState(breeds: ResultWrapper<List<BreedEntity>>?){
        when(breeds){
            is ResultWrapper.Loading -> {
                DisplayLoading()
            }
            is ResultWrapper.Success -> {
                DisplayAllDogBreeds(breeds.data,onClick = {breedEntity->
                    navigateToBreedPhotosScreen(breedEntity.breed)
                })
            }
            is ResultWrapper.Error -> {
                EmptyScreen(breeds)
            }
            else ->{}
        }

    }

    private fun navigateToBreedPhotosScreen(breed : String, photoAmount : Int? = Const.PHOTO_AMOUNT_DEFAULT){
        val bundle = Bundle().apply {
            putString(Const.BREED_KEY,breed)
            putInt(Const.PHOTO_AMOUNT_KEY,photoAmount!!)
        }

        val breedPhotoFragment = BreedPhotosScreen.breedPhotosScreenInstance()
        breedPhotoFragment.arguments = bundle
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container,breedPhotoFragment)
            .addToBackStack(BreedPhotosScreen::class.java.name)
            .commit()
    }
}