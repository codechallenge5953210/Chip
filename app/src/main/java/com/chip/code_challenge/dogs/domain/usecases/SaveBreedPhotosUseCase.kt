package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.dao.BreedPhotoDao
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity


class SaveBreedPhotosUseCase(private val breedPhotoDao: BreedPhotoDao) {

    suspend operator fun invoke(breedPhotos: BreedPhotoEntity) {
        breedPhotoDao.insert(breedPhotos)
    }
}