package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.dao.BreedsDao
import com.chip.code_challenge.dogs.data.local.model.BreedEntity

class SaveBreedsUseCase(private val breedsDao: BreedsDao) {

    suspend operator fun invoke(breeds : List<BreedEntity>){
        return breedsDao.insert(breeds)
    }
}