package com.chip.code_challenge.dogs.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class BreedEntity(
    @PrimaryKey
    val breed: String,
    val subBreeds : List<String>?
)
