package com.chip.code_challenge.dogs.data.repository

import com.chip.code_challenge.dogs.data.local.DogsDatabase
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import com.chip.code_challenge.dogs.data.remote.api.DogsApi
import com.chip.code_challenge.dogs.domain.repository.DogsRepository
import com.chip.code_challenge.dogs.utils.ResultWrapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DogsRepositoryImpl(
    private val dogsApi: DogsApi,
    private val dogsDatabase: DogsDatabase
) : DogsRepository {


    override fun fetchAllDogBreedsFromApi() = flow {
        val response = dogsApi.getAllDogBreeds()
        try {
            emit(ResultWrapper.Loading)
            val body = response.body()
            if (response.isSuccessful && body != null) {
                emit(ResultWrapper.Success(data = body))
            } else {
                emit(ResultWrapper.Error(response.message()))
            }
        } catch (ex: Exception) {
            emit(ResultWrapper.Error(ex.message))
        }
    }


    override fun fetchBreedPhotosFromApi(breed: String, photoAmount: Int) = flow {
        val response = dogsApi.getBreedPhotos(breed, photoAmount)
        try {
            emit(ResultWrapper.Loading)
            val body = response.body()
            if (response.isSuccessful && body != null) {
                emit(ResultWrapper.Success(data = body))
            } else {
                emit(ResultWrapper.Error(response.message()))
            }
        } catch (ex: Exception) {
            emit(ResultWrapper.Error(ex.message))
        }
    }

    override suspend fun saveAllBreedsToDatabase(breeds: List<BreedEntity>) {
        dogsDatabase.breedsDao.insert(breeds)
    }

    override suspend fun saveAllBreedPhotosToDatabase(breedPhotoEntity: BreedPhotoEntity) {
        dogsDatabase.breedPhotoDao.insert(breedPhotoEntity)
    }

    override fun getSavedBreedsFromDatabase(): Flow<List<BreedEntity>> {
        return dogsDatabase.breedsDao.getAllBreeds()
    }

    override fun getSavedBreedPhotosFromDatabase(breed: String): Flow<BreedPhotoEntity?> {
        return dogsDatabase.breedPhotoDao.getSelectedBreedPhotos(breed)
    }
}