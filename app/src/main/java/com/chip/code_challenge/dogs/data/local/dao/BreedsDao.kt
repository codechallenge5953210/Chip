package com.chip.code_challenge.dogs.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BreedsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(breeds : List<BreedEntity>)

    @Query("SELECT * FROM BreedEntity")
    fun getAllBreeds() : Flow<List<BreedEntity>>

}