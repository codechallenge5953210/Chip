package com.chip.code_challenge.dogs.di

import android.app.Application
import androidx.room.Room
import com.chip.code_challenge.dogs.data.local.DogsDatabase
import com.chip.code_challenge.dogs.data.local.convertors.DogsDBTypeConvertor
import com.chip.code_challenge.dogs.data.local.dao.BreedPhotoDao
import com.chip.code_challenge.dogs.data.local.dao.BreedsDao
import com.chip.code_challenge.dogs.utils.Const
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val databaseModule = module {

    fun provideDatabase(application: Application): DogsDatabase {
        return Room.databaseBuilder(
            application,
            DogsDatabase::class.java, Const.DATABASE_NAME
        )
            .addTypeConverter(DogsDBTypeConvertor())
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideDogsDao(database: DogsDatabase): BreedsDao = database.breedsDao
    fun provideBreedsPhotoDao(database: DogsDatabase) : BreedPhotoDao = database.breedPhotoDao

    single { provideDatabase(androidApplication()) }
    single { provideDogsDao(get()) }
    single { provideBreedsPhotoDao(get()) }
}
