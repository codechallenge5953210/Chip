package com.chip.code_challenge.dogs.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chip.code_challenge.dogs.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class DogBreedsActivity : AppCompatActivity() {

    private val viewModel : DogsViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dogbreeds)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container,DogBreedsFragment.dogBreedInstance())
            .addToBackStack(DogBreedsFragment::class.java.name)
            .commit()
    }
}