package com.chip.code_challenge.dogs.data.remote.api

import com.chip.code_challenge.dogs.data.local.model.BreedImageResponse
import com.chip.code_challenge.dogs.data.local.model.DogsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface DogsApi {

    @GET("breeds/list/all")
    suspend fun getAllDogBreeds() : Response<DogsResponse>

    @GET("breed/{breed}/images/random/{photo_amount}")
    suspend fun getBreedPhotos(@Path("breed") breed : String,
                               @Path("photo_amount") photoAmount : Int) : Response<BreedImageResponse>
}