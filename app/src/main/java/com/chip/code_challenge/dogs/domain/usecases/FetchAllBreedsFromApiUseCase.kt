package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.model.DogsResponse
import com.chip.code_challenge.dogs.domain.repository.DogsRepository
import com.chip.code_challenge.dogs.utils.ResultWrapper
import kotlinx.coroutines.flow.Flow

class FetchAllBreedsFromApiUseCase(private val dogsRepository: DogsRepository) {

     operator fun invoke() : Flow<ResultWrapper<DogsResponse>> {
        return dogsRepository.fetchAllDogBreedsFromApi()
    }
}