package com.chip.code_challenge.dogs.presentation.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.chip.code_challenge.dogs.R
import com.chip.code_challenge.dogs.data.local.model.BreedEntity

@Composable
fun DisplayAllDogBreeds(breeds: List<BreedEntity>, onClick: ((breed : BreedEntity) -> Unit)){
    LazyColumn(modifier = Modifier.padding(8.dp))
    {
        items(breeds){breed->
            BreedsCard(dogsBreed = breed ,onClick = {onClick(breed)})
        }
    }
}

@Composable
fun BreedsCard(dogsBreed: BreedEntity,
               onClick: (() -> Unit)? = null){
    OutlinedCard(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
            .clickable { onClick?.invoke() },
        colors = CardDefaults.cardColors(
            containerColor = colorResource(id = R.color.colorPrimary)
        ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .height(60.dp)
                .fillMaxWidth()
        ) {
            Text(text = dogsBreed.breed, style = MaterialTheme.typography.headlineMedium, maxLines = 1)

        }
    }
}