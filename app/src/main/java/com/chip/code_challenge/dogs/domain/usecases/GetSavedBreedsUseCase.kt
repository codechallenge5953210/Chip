package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.dao.BreedsDao
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import kotlinx.coroutines.flow.Flow

class GetSavedBreedsUseCase(private val breedsDao : BreedsDao) {

    operator fun invoke() : Flow<List<BreedEntity>>{
        return breedsDao.getAllBreeds()
    }
}