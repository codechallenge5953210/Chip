package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.model.BreedImageResponse
import com.chip.code_challenge.dogs.domain.repository.DogsRepository
import com.chip.code_challenge.dogs.utils.ResultWrapper
import kotlinx.coroutines.flow.Flow


class FetchBreedPhotosFromApiUseCase(private val dogsRepository: DogsRepository){

    operator fun invoke(breed : String,photoAmount : Int) : Flow<ResultWrapper<BreedImageResponse>> {
        return dogsRepository.fetchBreedPhotosFromApi(breed,photoAmount)
    }
}