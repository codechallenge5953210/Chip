package com.chip.code_challenge.dogs.presentation

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import com.chip.code_challenge.dogs.presentation.ui.DisplayBreedsGallery
import com.chip.code_challenge.dogs.presentation.ui.DisplayLoading
import com.chip.code_challenge.dogs.presentation.ui.EmptyScreen
import com.chip.code_challenge.dogs.utils.Const
import com.chip.code_challenge.dogs.utils.ResultWrapper
import com.chip.code_challenge.dogs.utils.ResultWrapper.Error
import com.chip.code_challenge.dogs.utils.ResultWrapper.Loading
import com.chip.code_challenge.dogs.utils.ResultWrapper.Success
import org.koin.androidx.viewmodel.ext.android.activityViewModel


class BreedPhotosScreen : Fragment() {

    private val viewModel: DogsViewModel by activityViewModel()

    companion object {
        @JvmStatic
        fun breedPhotosScreenInstance() = BreedPhotosScreen()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        if (args != null) {
            val breed = args.getString(Const.BREED_KEY)
            val photoAmount = args.getInt(Const.PHOTO_AMOUNT_KEY)
            if (breed != null) {
                viewModel.getSavedBreedPhotosFromDb(breed, photoAmount)
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MaterialTheme {
                    val breedPhotos = viewModel.breedsImage.observeAsState().value
                    ObserveBreedsState(breedPhotos = breedPhotos)
                }
            }
        }
    }


    @Composable
    private fun ObserveBreedsState(breedPhotos: ResultWrapper<BreedPhotoEntity>?) {
        when (breedPhotos) {
            is Loading -> {
                DisplayLoading()
            }

            is Success -> {
                breedPhotos.data.let { breedList ->
                    val fullBreeds = fillEmptySpaces(breedList.imageUrls?.toMutableList())
                    if (fullBreeds != null) {
                        DisplayBreedsGallery(fullBreeds)
                    }
                }
            }

            is Error -> {
                EmptyScreen(breedPhotos)
            }

            else -> {}
        }

    }

}

private fun fillEmptySpaces(breedsPhotos: MutableList<String>?): List<String>? {
    if (breedsPhotos != null && breedsPhotos.size < 10) {
        val remainingEmptySize = 10 - breedsPhotos.count()
        val placeHolder = List(remainingEmptySize) {
            Uri.parse(Const.PLACEHOLDER_DRAWABLE).toString()
        }
        breedsPhotos.addAll(placeHolder)
    }
    return breedsPhotos
}
