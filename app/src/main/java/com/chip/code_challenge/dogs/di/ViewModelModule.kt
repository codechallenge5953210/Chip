package com.chip.code_challenge.dogs.di

import com.chip.code_challenge.dogs.presentation.DogsViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val viewModelModule = module {
    viewModelOf(::DogsViewModel)
}