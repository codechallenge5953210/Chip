package com.chip.code_challenge.dogs.data.local.model


import com.squareup.moshi.Json

data class BreedImageResponse(
    @Json(name = "message")
    val message: List<String>?,
    @Json(name = "status")
    val status: String?
){
    fun toBreedPhotoEntity(breed:String) : BreedPhotoEntity? {
      return message?.run{
            BreedPhotoEntity(breed,imageUrls = message)
        }
    }
}
