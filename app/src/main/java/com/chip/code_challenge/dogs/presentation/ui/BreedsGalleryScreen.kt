package com.chip.code_challenge.dogs.presentation.ui

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.chip.code_challenge.dogs.R

@Composable
fun DisplayBreedsGallery(breedsPhotos: List<String>) {
    LazyVerticalGrid(columns = GridCells.Fixed(2)) {
        items(breedsPhotos) { imageUrl ->
            DisplayBreedPhoto(imageUrl = imageUrl)
        }
    }
}


@Composable
fun DisplayBreedPhoto(imageUrl: String) {
    AsyncImage(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(8.dp),
        contentScale = ContentScale.FillBounds,
        contentDescription = null,
        model = imageUrl,
        error = painterResource(id = R.drawable.no_dog_sign)
    )
}