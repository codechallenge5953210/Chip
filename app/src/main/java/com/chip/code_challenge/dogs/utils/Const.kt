package com.chip.code_challenge.dogs.utils

object Const {

    const val BASE_URL = "https://dog.ceo/api/"
    const val DATABASE_NAME = "dogs-db"
    const val NETWORK_ERROR = "please check your internet connection"
    const val UNKNOWN_ERROR = "unknown error please check your internet connection or restart the app"
    const val PLACEHOLDER_DRAWABLE ="R.drawable.no_dog_sign"
    const val BREED_KEY ="breed"
    const val PHOTO_AMOUNT_KEY = "photoAmount"
    const val PHOTO_AMOUNT_DEFAULT = 10
}