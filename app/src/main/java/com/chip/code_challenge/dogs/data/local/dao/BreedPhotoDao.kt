package com.chip.code_challenge.dogs.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BreedPhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(breedPhotos : BreedPhotoEntity)

    @Query("SELECT * FROM BreedPhotoEntity WHERE breed=:breed")
    fun getSelectedBreedPhotos(breed : String) : Flow<BreedPhotoEntity?>
}