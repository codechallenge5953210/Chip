package com.chip.code_challenge.dogs.data.local.model


import com.squareup.moshi.Json

data class DogsResponse(
    @Json(name = "message")
    val message: Map<String, List<String>?>,
    @Json(name = "status")
    val status: String?
){
    fun toBreedsEntity() : List<BreedEntity>{
       return message.entries.map { entry ->
            BreedEntity(entry.key,entry.value)
        }
    }
}
