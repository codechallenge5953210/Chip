package com.chip.code_challenge.dogs.domain.usecases

import com.chip.code_challenge.dogs.data.local.dao.BreedPhotoDao
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import kotlinx.coroutines.flow.Flow

class GetSavedBreedPhotosUseCase(private val breedsPhotoDao: BreedPhotoDao) {

    operator fun invoke(breed:String) : Flow<BreedPhotoEntity?>{
       return breedsPhotoDao.getSelectedBreedPhotos(breed)
   }
}