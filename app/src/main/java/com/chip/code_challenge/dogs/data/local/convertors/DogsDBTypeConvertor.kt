package com.chip.code_challenge.dogs.data.local.convertors

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types


@ProvidedTypeConverter
class DogsDBTypeConvertor {

    val moshi: Moshi get() = Moshi.Builder().build()


    @TypeConverter
    fun stringListToJson(list: List<String>?): String {
        return if (list.isNullOrEmpty()) {
            "[]"
        } else {
            moshi.adapter<List<String>>(
                Types.newParameterizedType(
                    List::class.java,
                    String::class.javaObjectType
                )
            )
                .toJson(list) ?: "[]"
        }
    }

    @TypeConverter
    fun jsonToListOfString(json: String?): List<String> {
        return if (json.isNullOrBlank()) {
            listOf()
        } else {
            moshi.adapter<List<String>>(
                Types.newParameterizedType(
                    List::class.java,
                    String::class.javaObjectType
                )
            )
                .fromJson(json) ?: listOf()
        }
    }

}