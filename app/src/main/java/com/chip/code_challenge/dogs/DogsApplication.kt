package com.chip.code_challenge.dogs

import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.disk.DiskCache
import com.chip.code_challenge.dogs.di.apiModule
import com.chip.code_challenge.dogs.di.databaseModule
import com.chip.code_challenge.dogs.di.dogsRepoModule
import com.chip.code_challenge.dogs.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class DogsApplication : Application(), ImageLoaderFactory {


    override fun onCreate() {
        super.onCreate()

        /**
         * Start Koin dependency
         */
        startKoin {
            androidContext(this@DogsApplication)
            androidLogger(Level.ERROR)
            modules(listOf(apiModule,databaseModule,
                dogsRepoModule, viewModelModule))
        }
    }

    override fun newImageLoader(): ImageLoader {
        return ImageLoader.Builder(this)
            .crossfade(true)
            .placeholder(R.drawable.deafult_loading)
            .error(R.drawable.default_error)
            .fallback(R.drawable.no_dog_sign)
            .diskCache {
                DiskCache.Builder()
                    .directory(this.cacheDir.resolve("coil_cache"))
                    .maxSizePercent(0.02)
                    .build()
            }
            .build()
    }
}