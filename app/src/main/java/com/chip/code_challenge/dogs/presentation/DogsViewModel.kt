package com.chip.code_challenge.dogs.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chip.code_challenge.dogs.data.local.model.BreedEntity
import com.chip.code_challenge.dogs.data.local.model.BreedPhotoEntity
import com.chip.code_challenge.dogs.domain.usecases.FetchAllBreedsFromApiUseCase
import com.chip.code_challenge.dogs.domain.usecases.FetchBreedPhotosFromApiUseCase
import com.chip.code_challenge.dogs.domain.usecases.GetSavedBreedPhotosUseCase
import com.chip.code_challenge.dogs.domain.usecases.GetSavedBreedsUseCase
import com.chip.code_challenge.dogs.domain.usecases.SaveBreedPhotosUseCase
import com.chip.code_challenge.dogs.domain.usecases.SaveBreedsUseCase
import com.chip.code_challenge.dogs.utils.Const
import com.chip.code_challenge.dogs.utils.ResultWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import java.net.UnknownHostException

open class DogsViewModel(
    private val fetchAllBreedsFromApiUseCase: FetchAllBreedsFromApiUseCase,
    private val fetchBreedPhotosFromApiUseCase: FetchBreedPhotosFromApiUseCase,
    private val saveBreedsUseCase: SaveBreedsUseCase,
    private val getSavedBreedsUseCase: GetSavedBreedsUseCase,
    private val saveBreedPhotosUseCase: SaveBreedPhotosUseCase,
    private val getSavedBreedPhotosUseCase: GetSavedBreedPhotosUseCase

) : ViewModel() {

    private val _dogBreeds = MutableLiveData<ResultWrapper<List<BreedEntity>>>()
    val dogBreeds: LiveData<ResultWrapper<List<BreedEntity>>> = _dogBreeds

    private val _breedsImage = MutableLiveData<ResultWrapper<BreedPhotoEntity>>()
    val breedsImage: LiveData<ResultWrapper<BreedPhotoEntity>> = _breedsImage


    init {
        fetchBreedsFromApiAndSaveToDB()
    }

     private fun fetchBreedsFromApiAndSaveToDB(){
         _dogBreeds.value = ResultWrapper.Loading
        viewModelScope.launch {
            fetchAllBreedsFromApiUseCase.invoke().flowOn(Dispatchers.IO)
                .catch {error->
                    when(error){
                        is UnknownHostException -> {
                            _dogBreeds.postValue(ResultWrapper.Error(Const.NETWORK_ERROR))
                        }
                    }
                }.collect{dogs->
                    if (dogs is ResultWrapper.Success){
                        saveBreedsUseCase.invoke(dogs.data.toBreedsEntity())
                        getSavedBreedsFromDB()
                    }
                }
        }
    }


     fun getSavedBreedsFromDB(){
        viewModelScope.launch {
            getSavedBreedsUseCase.invoke().flowOn(Dispatchers.IO)
                .collect{breeds->
                    if (breeds.isEmpty()){
                        _dogBreeds.value = ResultWrapper.Error(Const.NETWORK_ERROR)
                    }else{
                        _dogBreeds.value = ResultWrapper.Success(breeds)
                    }
                }
        }
    }


    private fun fetchBreedPhotosFromApi(breed:String, photoAmount : Int){
          viewModelScope.launch {
              fetchBreedPhotosFromApiUseCase.invoke(breed,photoAmount).flowOn(Dispatchers.IO)
                  .catch {error->
                      when(error){
                          is UnknownHostException -> {
                              _breedsImage.postValue(ResultWrapper.Error(Const.NETWORK_ERROR))
                          }
                      }
                  }.collect{breedImages->
                      if (breedImages is ResultWrapper.Success){
                          breedImages.data.toBreedPhotoEntity(breed)
                              ?.let {
                                  saveBreedPhotosUseCase.invoke(it)
                                  getSavedBreedPhotosFromDb(breed,photoAmount)
                              }

                      }
                  }
          }

    }

    fun getSavedBreedPhotosFromDb(breed: String,photoAmount : Int){
        viewModelScope.launch {
            getSavedBreedPhotosUseCase.invoke(breed).flowOn(Dispatchers.IO)
                .collect{breedPhotos->
                    if (breedPhotos == null || breedPhotos.imageUrls.isNullOrEmpty()){
                        fetchBreedPhotosFromApi(breed,photoAmount)
                    }else{
                        _breedsImage.value = ResultWrapper.Success(breedPhotos)
                    }
                }
        }
    }

}